/**
 * Dependencies
 * @ignore
 */

/**
 * Actions
 * @ignore
 */
export const ACTION_COUNTER_INCREMENT = 'INCREMENT'
export const incrementCounter = () => ({
  type: ACTION_COUNTER_INCREMENT,
})

export const ACTION_COUNTER_DECREMENT = 'DECREMENT'
export const decrementCounter = () => ({
  type: ACTION_COUNTER_DECREMENT,
})

export const ACTION_COUNTER_BOOST = 'BOOST'
export const boostCounter = (payload = 1) => ({
  type: ACTION_COUNTER_BOOST,
  payload,
})

export const ACTION_COUNTER_ASYNC_BOOST = 'ASYNC_BOOST'
export const delayedBoostCounter = (value = 1, delay = 1000) => ({
  type: ACTION_COUNTER_ASYNC_BOOST,
  payload: { value, delay },
})
