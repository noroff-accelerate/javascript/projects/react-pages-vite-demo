/**
 * Dependencies
 * @ignore
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider as ReduxProvider } from 'react-redux'

/**
 * Module Dependencies
 * @ignore
 */
import './index.css'
import store from './store'
import App from './App'

/**
 * Setup
 * @ignore
 */
ReactDOM.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <App />
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
