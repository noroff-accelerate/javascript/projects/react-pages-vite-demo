/**
 * Dependencies
 * @ignore
 */
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom'
import LandingPage from './views/LandingPage'
import Hello from './views/Hello'
import logo from './logo.svg'
import './App.css'

/**
 * Component
 * @ignore
 */
function App() {
  const basename =
    process.env.NODE_ENV === 'production'
      ? '/javascript/projects/react-pages-vite-demo/'
      : '/'

  return (
    <BrowserRouter basename={basename}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <ul>
            <li>
              <NavLink className="LandingPage-link" to="/">
                Root
              </NavLink>
            </li>
            <li>
              <NavLink className="LandingPage-link" to="/hello">
                Hello
              </NavLink>
            </li>
          </ul>
          <Routes>
            <Route path="/" element={<LandingPage />}></Route>
            <Route path="/hello" element={<Hello />}></Route>
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  )
}

/**
 * Exports
 * @ignore
 */
export default App
