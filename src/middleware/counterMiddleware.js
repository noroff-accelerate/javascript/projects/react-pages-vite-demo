/**
 * Dependencies
 * @ignore
 */

import { ACTION_COUNTER_ASYNC_BOOST, boostCounter } from '../actions'

/**
 * Module Dependencies
 * @ignore
 */

/**
 * Middleware
 * @ignore
 */
const counterMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action = {}) => {
    // next _MUST_ be called first in the middleware
    next(action)

    // Then you can do your middleware logic
    if (action?.type === ACTION_COUNTER_ASYNC_BOOST) {
      setTimeout(
        () => dispatch(boostCounter(action?.payload?.value)),
        action?.payload?.delay || 1000 // Try use supplied delay or default to 1000
      )
    }
  }

/**
 * Exports
 * @ignore
 */
export { counterMiddleware }
export default counterMiddleware
