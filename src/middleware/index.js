/**
 * Dependencies
 * @ignore
 */
import { applyMiddleware } from 'redux'

/**
 * Module Dependencies
 * @ignore
 */
import counterMiddleware from './counterMiddleware'

/**
 * Reducer
 * @ignore
 */
const appMiddleware = applyMiddleware(counterMiddleware)

/**
 * Exports
 * @ignore
 */
export default appMiddleware
