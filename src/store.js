/**
 * Dependencies
 * @ignore
 */
import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

/**
 * Module Dependencies
 * @ignore
 */
import appReducers from './reducers'
import appMiddleware from './middleware'

/**
 * Store
 * @ignore
 */
const store = createStore(appReducers, composeWithDevTools(appMiddleware))

/**
 * Exports
 * @ignore
 */
export default store
