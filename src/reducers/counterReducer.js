/**
 * Dependencies
 * @ignore
 */

/**
 * Module Dependencies
 * @ignore
 */
import {
  ACTION_COUNTER_INCREMENT,
  ACTION_COUNTER_DECREMENT,
  ACTION_COUNTER_BOOST,
} from '../actions/counterActions'

/**
 * Reducer
 * @ignore
 */
const counterReducer = (state = 0, action = {}) => {
  switch (action.type) {
    case ACTION_COUNTER_INCREMENT:
      return state + 1
    case ACTION_COUNTER_DECREMENT:
      return state - 1
    case ACTION_COUNTER_BOOST:
      // Destructure payload with default 1 if it doesn't exist.
      const { payload = 1 } = action
      return state + payload
  }

  return state
}

/**
 * Exports
 * @ignore
 */
export { counterReducer }
export default counterReducer
