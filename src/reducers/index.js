/**
 * Dependencies
 * @ignore
 */
import { combineReducers } from 'redux'

/**
 * Module Dependencies
 * @ignore
 */
import counterReducer from './counterReducer'

/**
 * Reducer
 * @ignore
 */
const appReducers = combineReducers({
  counter: counterReducer,
})

/**
 * Exports
 * @ignore
 */
export default appReducers
