/**
 * Component
 * @ignore
 */
function Hello() {
  return <h1>Hello, World!</h1>
}

/**
 * Exports
 * @ignore
 */
export default Hello
