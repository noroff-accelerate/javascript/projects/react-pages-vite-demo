/**
 * Dependencies
 * @ignore
 */
import { useSelector, useDispatch } from 'react-redux'

/**
 * Module Dependencies
 * @ignore
 */
import './LandingPage.css'
import {
  incrementCounter,
  decrementCounter,
  boostCounter,
  delayedBoostCounter,
} from '../actions'

/**
 * Component
 * @ignore
 */
function LandingPage() {
  const count = useSelector((state) => state.counter)
  const dispatch = useDispatch()

  return (
    <>
      <p>Hello Vite + React!</p>
      <p>
        <button type="button" onClick={() => dispatch(incrementCounter())}>
          count is: {count}
        </button>
        <button type="button" onClick={() => dispatch(decrementCounter())}>
          Decrement
        </button>
        <button type="button" onClick={() => dispatch(boostCounter(5))}>
          Boost by 5
        </button>
        <button
          type="button"
          onClick={() => dispatch(delayedBoostCounter(10, 1000))}
        >
          Boost by 10 in 1s
        </button>
      </p>
      <p>
        Edit <code>LandingPage.jsx</code> and save to test HMR updates.
      </p>
      <p>
        <a
          className="LandingPage-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {' | '}
        <a
          className="LandingPage-link"
          href="https://vitejs.dev/guide/features.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          Vite Docs
        </a>
      </p>
    </>
  )
}

/**
 * Exports
 * @ignore
 */
export default LandingPage
